# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

DOCKER_COMPOSE_OPTS=-f ./docker-compose.yml
DOCKER_COMPOSE_RUN_FLAGS=--entrypoint "" --rm --no-deps -T

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

build: ## Build up the project
	docker-compose $(DOCKER_COMPOSE_OPTS) up -d  --build

start: ## Start all containers
	@docker-compose $(DOCKER_COMPOSE_OPTS) up -d  --remove-orphans

ps: ## Spin up the project
	docker-compose $(DOCKER_COMPOSE_OPTS) ps

stop: ## Stop all containers
	docker-compose $(DOCKER_COMPOSE_OPTS) stop

down: ## remove all containers and networks on the machine
	docker-compose $(DOCKER_COMPOSE_OPTS) down

images:
	docker images

del-images:
	docker system prune -a

destroy: ##
	docker-compose $(DOCKER_COMPOSE_OPTS) stop
	docker-compose $(DOCKER_COMPOSE_OPTS) rm -f

boum: ## destroy / remove all containers on the machine
	for f in $$(docker ps -a -q); do docker stop $$f; done
	docker system prune --volumes -f