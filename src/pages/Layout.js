import React from 'react';
import { Button } from 'react-bootstrap';
import { Outlet, Link } from "react-router-dom";
import { Provider } from 'react-redux';
import { store } from '../storage/slices/score';

if(process.env.ONLY_DEV){
  console.log('This is only gonna be visible on DEV!');
}


const Layout = () => {
  return (
    <Provider store={store}>
      <nav className='gameplayed'>
        <ul>
          <li>
            <Button variant='dark'><Link to="/">PAIR GAME</Link></Button>
          </li>
          <li>
            <Button variant='dark'><Link to="/about">ABOUT</Link></Button>
          </li>
          <li>
            <Button variant='dark'><Link to="/score">BEST SCORE</Link></Button>
          </li>
        </ul>
      </nav>
      <Outlet />
    </Provider>
  )
};

export default Layout;