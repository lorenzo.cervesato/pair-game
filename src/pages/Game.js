import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import {useDispatch} from 'react-redux';
import LecteurMp3 from '../components/LecteurMp3';
import MySvg1 from '../img/bell-pepper.svg';
import MySvg2  from '../img/clef.svg';
import MySvg3 from '../img/giraffe.svg';
import MySvg4 from '../img/gun.svg';
import MySvg5 from '../img/message-bottle.svg';
import MySvg6 from '../img/mummy.svg';
import MySvg7 from '../img/pencils.svg';
import MySvg8 from '../img/pumpkin.svg';
import MySvg9 from '../img/santa.svg';
import MySvg10  from '../img/tiger.svg';
import {addNewScore} from '../storage/slices/score';
import Popup from '../components/Popup';


const jeudecartes1 = [
    {v:1,p:true, img:MySvg1},
    {v:2,p:true, img:MySvg2},
    {v:3,p:true, img:MySvg3},
    {v:4,p:true, img:MySvg4},
    {v:5,p:true, img:MySvg5},
    {v:6,p:true, img:MySvg6},
    {v:7,p:true, img:MySvg7},
    {v:8,p:true, img:MySvg8},
    {v:9,p:true, img:MySvg9},
    {v:10,p:true, img:MySvg10}
];
const jeudecartes2 = _.slice(jeudecartes1); // utilisation lodash
const jeudecartesComplet = _.concat(jeudecartes1,jeudecartes2); // utilisation lodash
const jeudecarteMelange = _.shuffle(jeudecartesComplet); // utilisation lodash

const Game = () => {
    const [stateJeu,setStateJeu] = useState(jeudecarteMelange); // Un tableau d'objets
    const [stepJeu, setStep] = useState(0); // Incrément de int
    const [indexFlipped, setIndexFlipped] = useState([]);
    const [indexValidated, setIndexValidated] = useState([]);
    const [countEssai, setCountEssai] = useState(0);
    const [isOpen, setIsOpen] = useState(false);
    const [inputValue, setInputValue] = useState("");
    const dispatch = useDispatch();

    useEffect(() => {
        if (stepJeu === 2) {
            togglePopup();
        }
    }, [stepJeu]);

    const togglePopup = () => {
        setIsOpen(!isOpen);
    }

    const init = () => {
        const jeuBack = stateJeu.map((item, i) => {
            return { ...item, p: !item.p };
        });

        setTimeout(() => {
            setStateJeu(jeuBack);
            setStep(stepJeu + 1);
          }, "5000");     
    }

    const partie = () => {
        var count = 0;
        stateJeu.forEach((item, i) => {
            indexValidated.forEach((item2,i2) => {
                if(i === parseInt(item2)){
                    count++;
                }
            })});
        if(count === 20){
            setStep(stepJeu + 1);
        }
    }

    switch(stepJeu){
        case 0: // Initialisation.
            init();
            break;
        case 1: // Partie lancée
            // Les différents clicks vont générer différents rechargement successifs en fonctions des mises à jour nécessaires au comportement recherché.
            // La plupart du code se trouve dans la fonction handle click.
            partie();
            break;
        case 2: // Gagné
            break;
        default: // Error
            break;
    }

    const handleNewGame = () => {
        setIndexValidated([]);
        setStateJeu(_.shuffle(stateJeu));
        setStep(0);
        setCountEssai(0);
    }

    const handleClick = (event) => {

        let arrNewIndex = indexFlipped;
        arrNewIndex.push(event.target.id);
       
        setIndexFlipped(arrNewIndex);
        let cardFlipped = indexFlipped.length;
        
        setStateJeu(
            stateJeu.map((item, i) =>
                i === parseInt(event.target.id)
                    ? { ...item, p: !item.p }
                    : { ...item }
            )
        );

        if(cardFlipped > 0 && cardFlipped % 2 === 0) {
            setCountEssai(countEssai + 1);
            if(stateJeu[indexFlipped[indexFlipped.length-2]].v !== stateJeu[indexFlipped[indexFlipped.length-1]].v){
                arrNewIndex = _.dropRight(arrNewIndex, 2);
                setIndexFlipped(arrNewIndex);
                setTimeout(() => {
                    setStateJeu(
                        stateJeu.map((item, i) => {
                            return (parseInt(indexFlipped[indexFlipped.length-2]) === i || parseInt(indexFlipped[indexFlipped.length-1]) === i )
                            ? { ...item, p: false }
                            : { ...item }
                        }  
                        )
                    );
                  }, "500");
            }

            if(arrNewIndex.length>0){
                let updateValidated = _.concat(indexValidated,arrNewIndex); // utilisation lodash
                setIndexValidated(updateValidated);
                arrNewIndex = [];
                setIndexFlipped(arrNewIndex);
            }

        }

    }

    const handleSave = () => {
        if(inputValue !== ''){
            const timeElapsed = Date.now();
            const today = new Date(timeElapsed);

            dispatch(
                addNewScore({
                    nom : inputValue,
                    score : countEssai,
                    successdate: today.toUTCString()
                })
            );
            console.log(inputValue);
            console.log(countEssai);
            togglePopup(); //Referme le popup
        }
    }

    var cardW = "90px";
    var cardH = "110px";

    return <Card className="card_container" variant="outlined">
        <Card.Header>
            <h2>{parseInt(stepJeu)===0?'Mémoirisez pendant 5 sec': parseInt(stepJeu)===1?'Retrouvez les paires':'Partie Gagnée!'}</h2>
            {parseInt(stepJeu)===1?<h3>Nombre Essai : {countEssai}</h3>:''}
            {parseInt(stepJeu)===2?<><h3>Nombre Essai : {countEssai}</h3><Button variant='success' onClick={handleNewGame}>Nouvelle Partie ?</Button></>:''}
        </Card.Header>
        <hr></hr>
        <LecteurMp3 autoplay={true}/>
        <hr></hr>
        <div className='game_container'>
        {stateJeu.map((val,i) => <Card onClick={(e) => (val.p === false && stepJeu < 2 && indexFlipped.length < 2)?handleClick(e):null} className={val.p?'front':'back'} id={i} key={"carte"+(i+1)}><p><img alt="imgSVG" width={cardW} height={cardH} src={val.img}/></p></Card>)}
        </div>
        <div>
            {isOpen && <Popup handleSave={handleSave} handleChange={setInputValue} handleClose={togglePopup} />}
        </div>

    </Card>;
}


export default Game;