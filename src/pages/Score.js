import { slice } from 'lodash';
import React from 'react';
import { Card } from 'react-bootstrap';
import { useSelector } from 'react-redux';

const Score = () => {
    const data = useSelector((state) => state.myscore);
    var data_copy = slice(data);
    var test = data_copy.sort((a, b) => (a.score > b.score) ? 1 : -1);
    

    return <Card className="card_container" variant="outlined">
        <Card.Header className='scoretitle'><h2 className='gameplayed'> HALL OF FAME </h2></Card.Header>
        <p>( RAPPEL: 10 COUPS MIN )</p>
        <ul className="list-group">
        {data_copy.map((score, index) => <><li key={'score'+index} className='list-group-item list-group-item-success'>{index+1}<span>Nom: {score.nom} - Date: {score.successdate}</span><span>Score: {score.score}</span></li></>)}
        </ul>
    </Card>;
}

export default Score;