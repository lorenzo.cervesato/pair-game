import React from 'react';
import { Card } from 'react-bootstrap';

const About = () => {
    return <Card className="card_container" variant="outlined">
        <Card.Header><h2>ABOUT</h2></Card.Header>
        <p>Made by Laurent Cervesato, Developper Web and Mobile.</p>
        <p>Email: lorenzo.cervesato@gmail.com</p>
    </Card>;
}
export default About;