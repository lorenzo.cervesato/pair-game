import React from "react";
import { Button } from "react-bootstrap";
 
const Popup = (props) => {
  return (
    <div className="popup-box">
      <div className="box">
        <span className="close-icon" onClick={props.handleClose}>x</span>
        <div className="popup-content">
        <h3>Bravo! Entrez votre Nom!</h3>
        <input className="myname" type='text' onChange={(event) => props.handleChange(event.target.value)} placeholder="Entrez votre nom" />
        <Button onClick={props.handleSave}>Enregistrer</Button>
        </div>
      </div>
    </div>
  );
};
 
export default Popup;