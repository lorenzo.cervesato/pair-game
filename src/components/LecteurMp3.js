import React, { useState, useEffect } from "react";
import MyMp3 from "./cpc-music-metro-cross.mp3";

function LecteurMp3() {
 
    // use Audio constructor to create HTMLAudioElement
    const audioTune = new Audio(MyMp3);
   
    // variable to play audio in loop
    const [playInLoop, setPlayInLoop] = useState(true);
   
    // load audio file on component load
    useEffect(() => {
      audioTune.load();
    }, [])
   
    // set the loop of audio tune
    useEffect(() => {
      audioTune.loop = playInLoop;
    }, [playInLoop])
   
    // play audio sound
    const playSound = () => {
      audioTune.play();
    }
   
    // pause audio sound
    const pauseSound = () => {
      audioTune.pause();
    }
   
    // stop audio sound
    const stopSound = () => {
      audioTune.pause();
      audioTune.currentTime = 0;
    }

    return (
      <div className="lecteur-mp3">
        <div>Music MP3 ---&gt; </div>
        <input type="button" className="btn btn-primary mr-2" value="Play" onClick={playSound}></input>
        <input type="button" className="btn btn-warning mr-2" value="Pause" onClick={pauseSound}></input>
        <input type="button" className="btn btn-danger mr-2" value="Stop" onClick={stopSound}></input>
        
        <div><label><input type="checkbox" checked={playInLoop} onChange={e => setPlayInLoop(e.target.checked)} /> Play in Loop</label></div>
      </div>
    );
  }
   
  export default LecteurMp3;