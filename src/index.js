import React from 'react';
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./pages/Layout";
import Game from "./pages/Game";
import About from "./pages/About";
import Score from "./pages/Score";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';


export default function App() {
  return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Game />} />
            <Route path="about" element={<About />} />
            <Route path="score" element={<Score />} />
          </Route>
        </Routes>
      </BrowserRouter>
  );
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);