import {createSlice, configureStore} from '@reduxjs/toolkit';

export const scoreSlice = createSlice({
    name : "myscore",
    initialState: [],
    reducers:{
        addNewScore: (state, action) => {
            state.push(action.payload);
        }
    }
});


export const store = configureStore({
    reducer: { 
      myscore: scoreSlice.reducer
    }
  });

export const {addNewScore} = scoreSlice.actions;

